import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { ActivityIndicator } from 'react-native';
import AuthStack from './navigation/AuthStack';

export default function DeepLinkUrl() {

  const linking = {
    // prefixes: ['deepLinkUrl://'],
    prefixes: ['deepLinkUrl://', 'https://deeplinkurl4.page.link'],
    config: {
      initialRouteName: 'Home',
      screens: {
        Home: {
          path: 'Home'
        },
        Settings: {
          path: 'Settings'
        },
        Profile: {
          path: 'Profile'
        }
      }
    }
  };

  return (
    <NavigationContainer linking={linking} fallback={<ActivityIndicator color="blue" size="large" />}>
      <AuthStack />
    </NavigationContainer>
  );
}