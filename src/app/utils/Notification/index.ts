import notifee from '@notifee/react-native';

class Notification {
  static displayNotification = async (title: string, body: string) => {
    try {
      // Request permissions (required for iOS)
      let permissionGrant = await notifee.requestPermission();

      let {android, authorizationStatus = 0, ios} = permissionGrant;

      if (authorizationStatus) {
        const channelId = await notifee.createChannel({
          id: 'default',
          name: 'Default Channel',
        });

        // Display a notification
        await notifee.displayNotification({
          title: title || 'Notification Title',
          body: body || 'Main body content of the notification',
          android: {
            channelId,
            // smallIcon: 'name-of-a-small-icon', // optional, defaults to 'ic_launcher'.
            // pressAction is needed if you want the notification to open the app when pressed
            pressAction: {
              id: 'default',
            },
          },
        });
      }
    } catch (e) {
      console.log('Display Notification Error', e);
    }
  };
}

export {Notification};
