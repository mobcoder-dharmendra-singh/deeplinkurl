import { Text, Touchable, TouchableOpacity, View } from 'react-native'
import React, { memo, useEffect, useState } from 'react'
import Styles from "./indexCss";
import dynamicLinks from '@react-native-firebase/dynamic-links';
import Clipboard from '@react-native-clipboard/clipboard';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

const ProfileScreen: React.FC = () => {

  const [counter, setCounter] = useState(0)
  const [counter2, setCounter2] = useState(0)


  const increment = () => {
    setCounter(counter + 1)
  }


  const increment2 = () => {
    setCounter2(counter2 + 1)
  }

  if (counter > 5) {
    throw new Error("Counter is greater than 5")
  }

  return (
    <View style={[Styles.container, Styles.centerContainer]}>

      <TouchableOpacity style={Styles.btnContainer} onPress={increment}>
        <Text style={Styles.btnTitle}>Counter</Text>
      </TouchableOpacity>

      <View style={Styles.spacingCss} />

      <Text style={Styles.btnTitle}>{counter}</Text>

      <View style={Styles.spacingCss} />

      <TouchableOpacity style={Styles.btnContainer} onPress={increment2}>
        <Text style={Styles.btnTitle}>Counter 2</Text>
      </TouchableOpacity>

      <View style={Styles.spacingCss} />

      <Text style={Styles.btnTitle}>{counter2}</Text>
    </View>
  )
}

export default memo(ProfileScreen);
