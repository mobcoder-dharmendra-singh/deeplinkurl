import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  centerContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  btnContainer: {
    paddingVertical: 20,
    paddingHorizontal: 40,
    backgroundColor: 'lightgreen'
  },
  btnTitle: {
    color: 'black',
  },
  spacingCss: {
    paddingVertical: 20
  }
})