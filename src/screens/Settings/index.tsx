import { Text, View } from 'react-native'
import React, { memo } from 'react'
import Styles from "./indexCss";

const SettingsScreen: React.FC = () => {
  return (
    <View style={[Styles.container, Styles.centerContainer]}>
      <Text>Setting Screen</Text>
    </View>
  )
}

export default memo(SettingsScreen);
