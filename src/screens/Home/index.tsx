import { Text, Touchable, TouchableOpacity, View } from 'react-native'
import React, { memo, useEffect, useState } from 'react'
import Styles from "./indexCss";
import dynamicLinks from '@react-native-firebase/dynamic-links';
import Clipboard from '@react-native-clipboard/clipboard';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

const HomeScreen: React.FC = () => {

  const navigation = useNavigation()

  const [generateLink, setGenerateLink] = useState('')
  const [copiedText, setCopiedText] = useState('');

  // Create a link
  const generateDeepLink = async () => {
    try {
      const link = await dynamicLinks().buildLink({
        link: 'https://invertase.io/settings',
        // domainUriPrefix is created in your Firebase console
        domainUriPrefix: 'https://deeplinkurl4.page.link',
        android: {
          packageName: 'com.deeplinkurl'
        }
      }, dynamicLinks.ShortLinkType.DEFAULT);

      setGenerateLink(link)
    } catch (e) {
      console.log("Error", JSON.stringify(e))
    }
  }

  const copyLink = async () => {
    Clipboard.setString(generateLink);
  }

  // Listen app in forground mode
  const handleDynamicLink = (link: any) => {
    try {
      // Handle dynamic link inside your own application
      if (link?.url === 'https://invertase.io/settings') {
        // ...navigate to your offers screen
        navigation.navigate("Settings")
      }
    } catch (e) {
      console.log("Dynamic Link error", e)
    }
  };

  useEffect(() => {
    const unsubscribe = dynamicLinks().onLink(handleDynamicLink);
    // When the component is unmounted, remove the listener
    return () => unsubscribe();
  }, []);

  //================================================

  // Listen app in Background  Mode
  useEffect(() => {
    dynamicLinks().getInitialLink().then((link: any) => {
      if (link?.url === 'https://invertase.io/settings') {
        // ...navigate to your offers screen
        navigation.navigate("Settings")
      }
    }).catch(e => { console.log("Background API dynamicLinks error") });
  }, []);



  return (
    <View style={[Styles.container, Styles.centerContainer]}>

      <TouchableOpacity style={Styles.btnContainer} onPress={generateDeepLink}>
        <Text style={Styles.btnTitle}>New Generate DeepLink</Text>
      </TouchableOpacity>

      <View style={Styles.spacingCss} />

      <Text style={Styles.btnTitle}>{generateLink}</Text>

      <View style={Styles.spacingCss} />

      <TouchableOpacity style={Styles.btnContainer} onPress={copyLink}>
        <Text style={Styles.btnTitle}>New Copy DeepLink</Text>
      </TouchableOpacity>
    </View>
  )
}

export default memo(HomeScreen);
