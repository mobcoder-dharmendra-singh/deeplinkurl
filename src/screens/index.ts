import Home from './Home';
import Settings from './Settings';
import Profile from './Profile';

export {Home, Settings, Profile};
