/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import type { PropsWithChildren } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import DeepLinkUrl from './src/DeepLinkUrl';
import ErrorBoundary from 'react-native-error-boundary';
import FallbackComponent from './src/FallbackComponent';


function App(): JSX.Element {

  const errorHandler = (error: Error, stackTrace: string) => {
    /* Log the error to an error reporting service */
    console.log("==ERROR==", error)
    console.log("==stackTrace==", stackTrace)

  }

  return (
    <ErrorBoundary FallbackComponent={FallbackComponent} onError={errorHandler}>
      <SafeAreaView style={styles.sectionContainer}>
        <DeepLinkUrl />
      </SafeAreaView>
    </ErrorBoundary>
  );
}

const styles = StyleSheet.create({
  sectionContainer: { flex: 1 },
});

export default App;
